# taskstats

A timing library for python. Record basic stats for arbitrary timed operations and compute average times
for repeated invocations.
